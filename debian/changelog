python-tmdbsimple (2.9.1-1) unstable; urgency=low

  * New upstream release.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Wed, 02 Feb 2022 11:50:56 +0000

python-tmdbsimple (2.8.1-1) unstable; urgency=low

  * New upstream release.
  * Use github tags instead of releases for d/watch.

 -- Michael Fladischer <fladi@debian.org>  Thu, 16 Dec 2021 09:03:48 +0000

python-tmdbsimple (2.8.0-1) unstable; urgency=low

  * New upstream release.
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.

 -- Michael Fladischer <fladi@debian.org>  Sun, 29 Aug 2021 20:37:09 +0000

python-tmdbsimple (2.7.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.5.1.
  * Use uscan version 4.

 -- Michael Fladischer <fladi@debian.org>  Wed, 02 Dec 2020 16:01:41 +0100

python-tmdbsimple (2.6.6-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Oct 2020 14:05:27 +0200

python-tmdbsimple (2.3.4-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * New upstream release.
  * Install README.md instead README.rst in binary package.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.

 -- Michael Fladischer <fladi@debian.org>  Tue, 30 Jun 2020 16:13:08 +0200

python-tmdbsimple (2.2.0-2) unstable; urgency=low

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Michael Fladischer ]
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.4.0.
  * Remove Python2 support.

 -- Michael Fladischer <fladi@debian.org>  Fri, 26 Jul 2019 10:40:29 +0200

python-tmdbsimple (2.2.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 4.1.4.

 -- Michael Fladischer <fladi@debian.org>  Fri, 01 Jun 2018 15:44:25 +0200

python-tmdbsimple (2.1.0-1) unstable; urgency=low

  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Enable autopkgtest-pkg-python testsuite.
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Tue, 09 Jan 2018 08:25:18 +0100

python-tmdbsimple (1.8.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.1.1.

 -- Michael Fladischer <fladi@debian.org>  Tue, 31 Oct 2017 10:29:16 +0100

python-tmdbsimple (1.7.0-1) unstable; urgency=low

  * New upstream release.
  * Upload to unstable.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Mon, 17 Jul 2017 10:17:07 +0200

python-tmdbsimple (1.6.1-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 17 Mar 2017 09:39:36 +0100

python-tmdbsimple (1.5.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 25 Jan 2017 11:37:17 +0100

python-tmdbsimple (1.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 11 Jan 2017 12:46:45 +0100

python-tmdbsimple (1.4.0-1) unstable; urgency=low

  * Initial release (Closes: #837884).

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Sep 2016 19:57:20 +0200
